<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pincode extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'office_name', 'pincode', '	office_type', 'delivery_status', 'division_name', 'region_name', 'circle_name', 'taluk', 'district_name', 'state_name',''
    ];
}
