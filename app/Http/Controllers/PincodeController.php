<?php

namespace App\Http\Controllers;

use App\Pincode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Jobs\CreateRecord;
use Session;


class PincodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pincode_data = Pincode::orderBy('created_at', 'desc')->paginate(10);
        return view('pincode.list', compact('pincode_data'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pincode.create');
    }

    /**
     * Return validation
     * @param $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validation($request)
    {
        $rules = [
            'importfile' =>  'required'
        ];

        $messages = [
            'importfile.required' => 'Please select file to import',
            'importfile.mimes' => 'File type .CSV allowed',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);

        return $validator;
    } 

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Return form validation.
        $validator = $this->validation($request);

        if($validator->fails()) {

            $messages = $validator->messages();
            return redirect()->back()->withInput($request->all())->withErrors($messages);

        } else {

               $path = Input::file('importfile')->getRealPath();
               $pincode_data = $this->csvToArray($path);
               $data = array();
               for ($i = 0; $i < count($pincode_data); $i ++)
                {
                    set_time_limit(120);

                    $data['office_name']        = $pincode_data[$i]['officename'];
                    $data['pincode']            = $pincode_data[$i]['pincode'];
                    $data['office_type']        = $pincode_data[$i]['officeType'];
                    $data['delivery_status']    = $pincode_data[$i]['Deliverystatus'];
                    $data['division_name']      = $pincode_data[$i]['divisionname'];
                    $data['region_name']        = $pincode_data[$i]['circlename'];
                    $data['circle_name']        = $pincode_data[$i]['Taluk'];
                    $data['taluk']              = $pincode_data[$i]['Districtname'];
                    $data['district_name']      = $pincode_data[$i]['statename'];
                    $data['state_name']         = $pincode_data[$i]['officename'];
                    
                    $is_exists = Pincode::where('pincode', $data['pincode'])->first();

                    //If record is not exixts in database then dispatch a job
                    if(!$is_exists) {
                       
                        // Dispatch a job.
                        dispatch(new CreateRecord($data));
                    }
                    
                }
                     
                return back()->with('success','Jobs dispatched successfully!');
        }
    }

    /**
     *  Create CSV to Array Data.
     * 
     */
    function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }


}
