@extends('layouts.master')
@section('title') List @endsection
@section('content')
<style>
    .row {
        margin-top: 10px;
    }
</style>
    <div class="container">
        
        <div class="row">
            <h3>Pincode Information</h3>
            <a href="{{url('/import-csv')}}" class="btn btn-success" style="position: absolute; right: 200px;">Back</a>
        </div>
        
        <div class="row">
        <div class="table-responsive">
            <table id="examplenew3" class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th >Office Name </th>
                        <th >Pincode</th>
                        <th >Office Type </th>
                        <th >Delivery Status </th>
                        <th >Division Name </th>
                        <th >Region Name </th>
                        <th >circle_name Name </th>
                        <th >Taluk </th>
                        <th >District Name </th>
                        <th >State Name </th>
                    </tr>
                </thead>
                <tbody id="result">
                    @forelse($pincode_data as $pincode)
                    <tr>
                        <td>{{$pincode->office_name}}</td>
                        <td>{{$pincode->pincode}}</td>
                        <td>{{$pincode->office_type}}</td>
                        <td>{{$pincode->delivery_status}}</td>
                        <td>{{$pincode->division_name}}</td>
                        <td>{{$pincode->region_name}}</td>
                        <td>{{$pincode->circle_name}}</td>
                        <td>{{$pincode->taluk}}</td>
                        <td>{{$pincode->district_name}}</td>
                        <td>{{$pincode->state_name}}</td>
                    </tr>
                    @empty
                    <tr >
                        <td colspan="10" class="text-center"> No records available!</td>
                    </tr>
                    @endforelse
                    
                </tbody> 
            </table>
        </div>
        {{$pincode_data->links()}}
    </div>
</div>

@endsection
@section('scripts')
@parent
@endsection('script')