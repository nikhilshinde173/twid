@extends('layouts.master')
@section('title') Import file @endsection
@section('content')
<style>
    .row {
        margin-top: 10px;
    }
</style>
    <div class="container">
    <div class="row">
		<div class="col-md-6 col-md-offset-2">
            <div class="panel panel-primary">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>	
                    <strong>{{ $message }}</strong>
            </div>
            @endif
                <div class="panel-heading">
                    <h3 class="panel-title">Import File</h3>
                </div>
                <div class="panel-body">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ url('/import-csv') }}" method="POST" role="form" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <label for="file">Select File (.CSV):</label>
                            <input type="file" class="form-control" name="importfile" style="padding: 3px;">
                        </div>
                       
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="{{url('/show-data')}}" class="btn btn-success">Go to List</a>
                    </form>
                </div>
            </div>
		</div>
	</div>
	
</div>

@endsection
@section('scripts')
@parent
@endsection('script')