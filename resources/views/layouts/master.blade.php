<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="" sizes="76x76" href="">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>
     Twid | @yield('title')
  </title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <!-- Datatables style -->
  <link rel="stylesheet" href="{{url('css/dataTables.bootstrap.min.css')}}">
  
</head>

<body>
  <nav class="navbar navbar-dark bg-dark">
    <div class="container-fluid">
      <div class="navbar-header">
        <div class="container clearfix"> 
          <div class="logo"><a href="{{ url('/import-csv') }}"><img src="https://twidpay.com/images/twid-logo.svg" alt=""></a></div>
      </div>
    </div>
  </nav>
      
      <!-- <div class="main-panel"> -->
      @yield('content')
    <div class="footer"></div>
    <!-- AdminLTE for datatables purposes -->
    <script src="{{url('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{url('js/dataTables.bootstrap.min.js')}}"></script>

    <script>
        $('#examplenew').DataTable({
          "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
          // "processing": true,
          // "serverSide": true,
          // "paging": true,
          // "searching": true,
      });
    </script>
  </div>

  <!--   Core JS Files   -->

</body>
</html>